#include "pcre2wrap/regex.hpp"
#include <array>
#include <cstdint>
#include <fmt/format.h>
#include <pcre2.h>
#include <stdexcept>
#include <string_view>

p2w::index_error::index_error(std::size_t index, std::size_t bound)
    : std::runtime_error(format_message(index, bound)), index(index),
      bound(bound) {
}

std::string p2w::index_error::format_message(std::size_t index,
                                             std::size_t bound) {
    return fmt::format("index {} is out of bounds (bound {})", index, bound);
}

p2w::ovector::ovector(std::size_t* ptr, std::size_t count) noexcept
    : m_ptr(ptr), m_count(count) {
}

std::size_t p2w::ovector::operator[](std::size_t index) const noexcept {
    return m_ptr[index];
}

std::size_t p2w::ovector::at(std::size_t index) const {
    if (index >= m_count * 2) {
        throw index_error{index, m_count * 2};
    }

    return m_ptr[index];
}

void p2w::match_data::deleter::operator()(
    pcre2_match_data* match_data) noexcept {
    pcre2_match_data_free(match_data);
}

p2w::match_data::match_data(pcre2_match_data* raw,
                            std::string_view text) noexcept
    : m_ptr(raw), m_text(text) {
}

p2w::ovector p2w::match_data::get_ovector() const noexcept {
    return p2w::ovector{pcre2_get_ovector_pointer(m_ptr.get()),
                        pcre2_get_ovector_count(m_ptr.get())};
}

std::string p2w::match_data::group(std::size_t g) const noexcept {
    return std::string{m_text.begin() + get_ovector()[g * 2],
                       m_text.begin() + get_ovector()[g * 2 + 1]};
}

void p2w::code::deleter::operator()(pcre2_code* code) noexcept {
    pcre2_code_free(code);
}

p2w::code::code(std::string_view pattern, std::uint32_t flags) {
    std::int32_t errcode;
    std::size_t errofs;
    m_code =
        ptr{pcre2_compile(reinterpret_cast<PCRE2_SPTR8>(pattern.data()),
                          pattern.size(), flags, &errcode, &errofs, nullptr)};
    if (m_code == nullptr) {
        std::array<PCRE2_UCHAR, 256> errbuf;
        pcre2_get_error_message(errcode, errbuf.data(), errbuf.size());
        throw compile_error{
            std::string{reinterpret_cast<const char*>(errbuf.data())}, pattern,
            errofs};
    }
}

std::optional<p2w::match_data> p2w::code::match(std::string_view text,
                                                std::uint32_t flags) const {
    pcre2_match_data* data =
        pcre2_match_data_create_from_pattern(m_code.get(), nullptr);
    int match_result =
        pcre2_match(m_code.get(), reinterpret_cast<PCRE2_SPTR>(text.data()),
                    text.size(), 0, flags, data, nullptr);
    if (match_result < 0) {
        if (match_result == PCRE2_ERROR_NOMATCH) {
            return {};
        }
        std::array<PCRE2_UCHAR, 256> errbuf;
        pcre2_get_error_message(match_result, errbuf.data(), errbuf.size());
        throw match_error{
            std::string{reinterpret_cast<const char*>(errbuf.data())}};
    }
    return match_data{data, text};
}

p2w::compile_error::compile_error(std::string&& message,
                                  std::string_view pattern, std::size_t offset)
    : std::runtime_error(format_message(message, pattern, offset)),
      message(std::move(message)), pattern(pattern), offset(offset) {
}

std::string p2w::compile_error::format_message(std::string_view message,
                                               std::string_view pattern,
                                               std::size_t offset) {
    return fmt::format("compiling '{}': {} (offset {})", pattern, message,
                       offset);
}

p2w::match_error::match_error(std::string&& message)
    : std::runtime_error(format_message(message)), message(std::move(message)) {
}

std::string p2w::match_error::format_message(std::string_view message) {
    return fmt::format("matching pattern: {}", message);
}
