#include "crossdir/crossdir.hpp"
#include <cstdlib>

#ifdef _WIN32
#else
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>
#endif

std::filesystem::path cd::home() {
    constexpr const char* fallback = "~";
#ifdef _WIN32
    const char* raw_user_home = std::getenv("USERPROFILE");
    if (raw_user_home == nullptr) {
        raw_user_home = std::getenv("HOMEPATH");
        if (raw_user_home == nullptr) {
            raw_user_home = fallback;
        }
    }
#else
    const char* raw_user_home = std::getenv("HOME");
    if (raw_user_home == nullptr) {
        raw_user_home = getpwuid(getuid())->pw_dir;
        if (raw_user_home == nullptr) {
            raw_user_home = fallback;
        }
    }
#endif
    return std::filesystem::path{raw_user_home};
}

std::filesystem::path cd::config() {
    const char* raw_xdg_config = std::getenv("XDG_CONFIG_HOME");
    if (raw_xdg_config == nullptr) {
        return home() / ".config";
    }
    return std::filesystem::path{raw_xdg_config};
}
