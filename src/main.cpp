#include <algorithm>
#include <crossdir/crossdir.hpp>
#include <cstddef>
#include <filesystem>
#include <fmt/format.h>
#include <fstream>
#include <functional>
#include <inipp.h>
#include <iterator>
#include <optional>
#include <pcre2wrap/regex.hpp>
#include <regex>
#include <span>
#include <stdexcept>
#include <string>
#include <string_view>
#include <vector>

struct io_error : std::runtime_error {
    io_error() : std::runtime_error("i/o error") {
    }
};

struct todo {
    std::string prefix;
    std::string suffix;
    std::optional<std::string> id;
    std::filesystem::path filename;
    std::size_t line;

    std::string string() const {
        if (id) {
            return fmt::format("{}:{}: {}TODO({}): {}", filename.string(), line,
                               prefix, *id, suffix);
        }
        return fmt::format("{}:{}: {}TODO: {}", filename.string(), line, prefix,
                           suffix);
    }

    void update() {
        // TODO: update() is not implemented
    }
};

struct github_credentials {
    std::string personal_token;

    static github_credentials
    from_file(const std::filesystem::path& file_path) {
        inipp::Ini<char> ini;
        std::ifstream in{file_path};
        ini.parse(in);
        std::string personal_token;
        inipp::get_value(ini.sections["github"], "personal_token",
                         personal_token);
        return github_credentials{.personal_token = personal_token};
    }
};

std::optional<todo> line_as_unreported_todo(std::string_view line) {
    static const p2w::code unreported_todo{R"re(^(.*)TODO: (.*)$)re", 0};
    std::optional<p2w::match_data> match_data = unreported_todo.match(line, 0);
    if (!match_data) {
        return {};
    }

    return todo{
        .prefix = match_data->group(1),
        .suffix = match_data->group(2),
        .id = std::nullopt,
        .filename = "",
        .line = 0,
    };
}

std::optional<todo> line_as_reported_todo(std::string_view line) {
    static const p2w::code reported_todo{R"re(^(.*)TODO\((.*)\): (.*)$)re", 0};
    std::optional<p2w::match_data> match_data = reported_todo.match(line, 0);
    if (!match_data) {
        return {};
    }

    return todo{
        .prefix = match_data->group(1),
        .suffix = match_data->group(3),
        .id = match_data->group(2),
        .filename = "",
        .line = 0,
    };
}

std::optional<todo> line_as_todo(std::string_view line) {
    if (std::optional<todo> t = line_as_unreported_todo(line); t) {
        return t;
    }
    if (std::optional<todo> t = line_as_reported_todo(line); t) {
        return t;
    }
    return {};
}

void walk_todos_of_file(const std::filesystem::path& file_path,
                        std::function<void(todo&&)> visit) {
    std::ifstream file{file_path};
    if (!file) {
        throw io_error{};
    }

    std::string line;
    while (std::getline(file, line)) {
        std::optional<todo> t = line_as_todo(line);
        if (t) {
            visit(std::move(*t));
        }
    }
}

void walk_todos_of_dir(const std::filesystem::path& dir_path,
                       std::function<void(todo&&)> visit) {
    for (std::filesystem::recursive_directory_iterator it{dir_path};
         it != std::filesystem::recursive_directory_iterator{}; ++it) {
        if (!it->is_regular_file()) {
            continue;
        }
        walk_todos_of_file(it->path(), visit);
    }
}

void list_subcommand() {
    walk_todos_of_dir(".", [](todo&& t) { fmt::print("{}\n", t.string()); });
}

todo report_todo(const todo& t, const github_credentials& creds) {
    return {};
}

void report_subcommand(const github_credentials& creds) {
    std::vector<todo> reported_todos;

    walk_todos_of_dir(".", [&reported_todos, creds](todo&& t) {
        if (!t.id) {
            todo reported_todo = report_todo(t, creds);
            fmt::print("[REPORTED] {}\n", t.string());
            reported_todos.emplace_back(std::move(reported_todo));
        }
    });

    for (todo& t : reported_todos) {
        t.update();
    }
}

int main(int argc, char** argv) {
    std::span args{argv, static_cast<std::size_t>(argc)};

    auto creds =
        github_credentials::from_file(cd::config() / "snitch" / "github.ini");

    if (args.size() > 1) {
        if (args[1] == std::string_view{"list"}) {
            list_subcommand();
        } else if (args[1] == std::string_view{"report"}) {
            report_subcommand(creds);
        } else {
            throw std::runtime_error{
                fmt::format("`{}` unknown command", args[1])};
        }
    } else {
        fmt::print("{} [opt]\n", args[0]);
        fmt::print("\tlist: lists all todos of a dir recursively\n");
        fmt::print("\treport: reports an issue to github\n");
    }
    return 0;
}
