#ifndef CROSSDIR_CROSSDIR_HPP_
#define CROSSDIR_CROSSDIR_HPP_

#include <filesystem>

namespace cd {

std::filesystem::path home();
std::filesystem::path config();

}

#endif
