#ifndef PCRE2WRAP_REGEX_HPP_
#define PCRE2WRAP_REGEX_HPP_

#include <cstddef>
#include <memory>
#include <optional>
#include <pcre2.h>
#include <stdexcept>
#include <string>
#include <string_view>

namespace p2w {

struct index_error : public std::runtime_error {
    std::size_t index;
    std::size_t bound;

    index_error(std::size_t index, std::size_t bound);

  private:
    std::string format_message(std::size_t index, std::size_t bound);
};

struct compile_error : public std::runtime_error {
    std::string message;
    std::string_view pattern;
    std::size_t offset;

    compile_error(std::string&& message, std::string_view pattern,
                  std::size_t offset);

  private:
    std::string format_message(std::string_view message,
                               std::string_view pattern, std::size_t offset);
};

struct match_error : public std::runtime_error {
    std::string message;

    match_error(std::string&& message);

  private:
    std::string format_message(std::string_view message);
};

class ovector {
    std::size_t* m_ptr;
    std::size_t m_count;

    friend class match_data;
    ovector(std::size_t* ptr, std::size_t count) noexcept;

  public:
    std::size_t operator[](std::size_t index) const noexcept;
    std::size_t at(std::size_t index) const;
};

class match_data {
    struct deleter {
        void operator()(pcre2_match_data* match_data) noexcept;
    };

    using ptr = std::unique_ptr<pcre2_match_data, deleter>;
    ptr m_ptr;

    friend class code;
    std::string_view m_text;
    match_data(pcre2_match_data* raw, std::string_view text) noexcept;

  public:
    ovector get_ovector() const noexcept;
    std::string group(std::size_t g) const noexcept;
};

class code {
    struct deleter {
        void operator()(pcre2_code* code) noexcept;
    };

    using ptr = std::unique_ptr<pcre2_code, deleter>;
    ptr m_code;

  public:
    code(std::string_view pattern, std::uint32_t flags);
    std::optional<p2w::match_data> match(std::string_view text,
                                         std::uint32_t flags) const;
};

} // namespace p2w

#endif
